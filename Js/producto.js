jQuery(document).ready(function() {
  $('#agregar-producto').on('click', function (e) {
    
    e.preventDefault();
    if($("#agregar-producto").hasClass("toogle")){

      $("#agregar-producto" ).removeClass( "toogle" );

      $("#formularioAgregarProducto").slideUp("slow");
      $("#tabla-productos").slideDown("slow");
      
    }else{

      $("#agregar-producto" ).addClass( "toogle" );

      $("#tabla-productos").slideUp("slow");
      $("#formularioAgregarProducto").slideDown("slow");
    }
  });
});