jQuery(document).ready(function() {
  $('#agregar-usuario').on('click', function (e) {
    
    e.preventDefault();
    if($("#agregar-usuario").hasClass("toogle")){

      $("#agregar-usuario" ).removeClass( "toogle" );
      $("#formulario-usuario").slideUp("slow");
      $("#tabla-usuarios").slideDown("slow");
      
    }else{
      
      $("#agregar-usuario" ).addClass( "toogle" );
      $("#tabla-usuarios").slideUp("slow");
      $("#formulario-usuario").slideDown("slow");
    }

  });
});