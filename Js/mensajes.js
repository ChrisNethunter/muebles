function mensaje(tipoMensaje){

  if ( tipoMensaje  == 1) {
    swal(
      'Buen Trabajo!',
      'Se Guardo el Producto Correctamente',
      'success'
    )
  }else if ( tipoMensaje  == 2 ){
    swal(
      'Error!',
      'Intenta de Nuevo!',
      'error'
    )
  }else if ( tipoMensaje  == 3 ){
    swal(
      'Lo Sentimos!',
      'Ocurrio un error subiendo la imagen!',
      'error'
    )
  }else if ( tipoMensaje  == 4 ){
    swal(
      'Lo Sentimos!',
      'Tu archivo no se ha podido subir',
      'error'
    )
  }else if ( tipoMensaje  == 5 ){
    swal(
      'Lo Sentimos!',
      'Tu archivo de imagen ya existe, cambia el nombre',
      'error'
    )

  }else if ( tipoMensaje  == 6 ){
    swal(
      'Lo Sentimos!',
      'Solo JPG, JPEG, PNG & GIF son formatos permitidos',
      'error'
    )
    
  }else if ( tipoMensaje  == 7 ){
    swal(
      'Buen Trabajo!',
      'Se elimino el producto Correctamente',
      'success'
    )
    
  }else if ( tipoMensaje  == 8 ){
    swal(
      'Lo Sentimos!',
      'Ha ocurrido un problema eliminando el producto!',
      'error'
    )
  }else if ( tipoMensaje  == 9 ){
    swal(
      'Buen Trabajo!',
      'Se Edito Correctamente el producto',
      'success'
    )
  }
      
}
