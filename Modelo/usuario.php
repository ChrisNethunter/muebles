<?php
class Usuario
{
	private $cedula;
	private $nombres;
	private $apellidos;
	private $correo;
	
	public function setCedula($cedula)
	{
		$this->cedula = $cedula;
	}
	
	public function getCedula()
	{
		return ($this->cedula);
	}
	
	public function setNombres($nombres)
	{
		$this->nombres = $nombres;
	}
	
	public function getNombres()
	{
		return ($this->nombres);
	}
	
	public function setApellidos($apellidos)
	{
		$this->apellidos = $apellidos;
	}
	
	public function getApellidos()
	{
		return ($this->apellidos);
	}
		
	public function setCorreo($correo)
	{
		$this->correo = $correo;
	}
	
	public function getCorreo()
	{
		return ($this->correo);
	}

	
	
	public function crearSolicitud($cedula,$nombres,$apellidos,$correo)
	{	

		$this->cedula         = $cedula;
		$this->nombres        = $nombres;
		$this->apellidos      = $apellidos;
		$this->correo         = $correo;		
	}
	
	public function consultarUsuarios()
	{
		$this->Conexion=Conectarse();
		$sql="SELECT * FROM usuarios";
		$resultado=$this->Conexion->query($sql);

		if ($resultado) {
			# code...
			$this->Conexion->close();
			return $resultado;	
			
		}else{
			return $this->Conexion->error;
		}
	}


	public function elminarUsuario($cedula)
	{	
		$cedula = intval($cedula);
		$this->Conexion=Conectarse();
		$sql= 'DELETE FROM usuarios WHERE `cedula` = '. $cedula;
		$resultado=$this->Conexion->query($sql);
		$this->Conexion->close();
		return $resultado;		
	}

}

?>