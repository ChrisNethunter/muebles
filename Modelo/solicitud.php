<?php
class Solicitud
{
	private $id;
	private $nombre;
	private $apellido;
	private $telefono;
	private $email;
	private $imagen;
	private $descripcion;
	private $fecha;
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return ($this->id);
	}
	
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}
	
	public function getNombre()
	{
		return ($this->nombre);
	}
	
	public function setApellido($apellido)
	{
		$this->apellido = $apellido;
	}
	
	public function getApellido()
	{
		return ($this->apellido);
	}
	
	public function setTelefono($telefono)
	{
		$this->telefono = $telefono;
	}
	
	public function getTelefono()
	{
		return ($this->telefono);
	}
	
	public function setEmail($email)
	{
		$this->email = $email;
	}
	
	public function getEmail()
	{
		return ($this->email);
	}

	public function setImagen($imagen)
	{
		$this->imagen = $imagen;
	}
	
	public function getImagen()
	{
		return ($this->imagen);
	}

	public function setDescripcion($descripcion)
	{
		$this->descripcion = $descripcion;
	}
	public function getDescripcion()
	{
		return($this->descripcion);
	}

	public function setFecha($fecha)
	{
		$this->fecha = $fecha;
	}
	
	public function getFecha()
	{
		return ($this->fecha);
	}
	
	public function crearSolicitud($nombre,$apellido,$telefono,$email,$imagen,$descripcion)
	{	

		$this->nombre         = $nombre;
		$this->apellido       = $apellido;
		$this->telefono       = $telefono;
		$this->email          = $email;
		$this->imagen    	  = $imagen;
		$this->descripcion    = $descripcion;		
	}
	
	public function agregarSolicitud()
	{	
		$this->Conexion = Conectarse();
       	$sql = "INSERT INTO solicitud (nombre,apellido,telefono,email,img,description,fecha_solicitud,status)" . "VALUES ('$this->nombre','$this->apellido','$this->telefono','$this->email','$this->imagen','$this->descripcion',NOW(),true)";

		$resultado = $this->Conexion->query($sql);
		$this->Conexion->close();
		return $resultado;
	}
	
	public function consultarSolicitudes()
	{
		$this->Conexion=Conectarse();
		$sql="SELECT * FROM solicitud";
		$resultado=$this->Conexion->query($sql);

		if ($resultado) {
			# code...
			$this->Conexion->close();
			return $resultado;	
			
		}else{
			return $this->Conexion->error;
		}
	}

}
?>