<?php
class Producto
{
	private $id;
	private $nombre;
	private $precio;
	private $cantidad;
	private $fecha;
	private $descripcion;
	private $imagen;
	
	public function setId($id)
	{
		$this->id = $id;
	}
	
	public function getId()
	{
		return ($this->id);
	}
	
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}
	
	public function getNombre()
	{
		return ($this->nombre);
	}
	
	public function setPrecio($precio)
	{
		$this->precio = $precio;
	}
	
	public function getPrecio()
	{
		return ($this->precio);
	}
	
	public function setCantidad($cantidad)
	{
		$this->cantidad = $cantidad;
	}
	
	public function getCantidad()
	{
		return ($this->cantidad);
	}
	
	public function setFecha($fecha)
	{
		$this->fecha = $fecha;
	}
	
	public function getFecha()
	{
		return ($this->fecha);
	}
	
	public function crearProducto($nombre,$precio,$cantidad,$descripcion,$imagen)
	{	

		$this->nombre         = $nombre;
		$this->precio         = $precio;
		$this->cantidad       = $cantidad;
		$this->descripcion    = $descripcion;
		$this->imagen    	  = $imagen;		
	}
	
	public function agregarProducto()
	{	
		$this->Conexion = Conectarse();

		$sql = "INSERT INTO productos (name,price,quantity,description, fecha_purchase,img_product,status)" 
            . "VALUES ('$this->nombre','$this->precio','$this->cantidad','$this->descripcion',NOW(),'$this->imagen', true)";

		$resultado=$this->Conexion->query($sql);
		$this->Conexion->close();
		return $resultado;
		
	}
	
	public function consultarProductos()
	{
		$this->Conexion=Conectarse();
		$sql="SELECT * FROM productos";
		$resultado=$this->Conexion->query($sql);

		if ($resultado) {
			# code...
			$this->Conexion->close();
			return $resultado;	
			
		}else{
			return $this->Conexion->error;
		}
	}
	
	public function consultarProducto($id)
	{
		$this->Conexion=Conectarse();
		$sql="SELECT * FROM productos WHERE id='$id'";
		$resultado=$this->Conexion->query($sql);
		$this->Conexion->close();
		return $resultado;	
	}

	public function actualizarProducto($id){
		$this->Conexion=Conectarse();
		if ($this->imagen != "") {
			$sql = "
				UPDATE
					productos
				SET
					name  = '$this->nombre',
					price = '$this->precio',
					quantity = '$this->cantidad',
					description  = '$this->descripcion',
					img_product  = '$this->imagen'
				WHERE
				 id = '$id'
			";
		}else{
			$sql = "
				UPDATE
					productos
				SET
					name  = '$this->nombre',
					price = '$this->precio',
					quantity = '$this->cantidad',
					description  = '$this->descripcion'
				WHERE
				 id = '$id'
			";
		}
		
		$resultado=$this->Conexion->query($sql);
		$this->Conexion->close();
		return $resultado;
	}

	public function elminarProducto($id)
	{	
		$id = intval($id);
		$this->Conexion=Conectarse();
		$sql= 'DELETE FROM productos WHERE `id` = '. $id;
		$resultado=$this->Conexion->query($sql);
		$this->Conexion->close();
		return $resultado;		
	}
}
?>