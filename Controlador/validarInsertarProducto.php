<?php
	session_start();
	extract($_REQUEST); //recoger todas las variables que pasan Método GET o POST
	$url = $_SERVER['DOCUMENT_ROOT']."/muebles/";
	require $url.'Modelo/conexionBasesDatos.php';
	require $url."Modelo/Producto.php";
	
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') 
  	{
  		$target_dir =  $url."uploads/";
		$target_file = $target_dir . basename($_FILES["imagen"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["imagen"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}

		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
		    //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		    header ("location:http://localhost/muebles/Vista/index2.php?pag=adminProductos&menu=1&msj=6");
		}

		// Check if file already exists
		if (file_exists($target_file)) {
		    //echo "Sorry, file already exists.";
		    $uploadOk = 0;
		    header ("location:http://localhost/muebles/Vista/index2.php?pag=adminProductos&menu=1&msj=5");
		}

		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    //echo "Sorry, your file was not uploaded.";
		    //header ("location:http://localhost:8080/muebles/Vista/index2.php?pag=adminProductos&menu=1&msj=4");
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {

		        //echo "The file ". basename( $_FILES["imagen"]["name"]). " has been uploaded.";
		    	$file = basename( $_FILES["imagen"]["name"]);
		    } else {
		    	header ("location:http://localhost/muebles/Vista/index2.php?pag=adminProductos&menu=1&msj=3");
		        //echo "Sorry, there was an error uploading your file.";
		    }
		}
  	}

  	if ($uploadOk == 1) {
  		$objProducto= new Producto();
		$objProducto->crearProducto($_REQUEST['nombre'],$_REQUEST['precio'],$_REQUEST['cantidad'],
		$_REQUEST['descripcion'], $file);
		$resultado = $objProducto->agregarProducto();
		if ($resultado)
			header ("location:http://localhost/muebles/Vista/index2.php?pag=adminProductos&menu=1&msj=1");
		else
			header ("location:http://localhost/muebles/Vista/index2.php?pag=adminProductos&menu=1&msj=2");		
  	}

	/*$nombre     	= $mysqli->escape_string($_REQUEST['nombre']);
    $precio 		= $mysqli->escape_string($_REQUEST['precio']);
    $cantidad  		= $mysqli->escape_string($_REQUEST['cantidad']);
    $descripcion    = $mysqli->escape_string($_REQUEST['descripcion']);
    $fecha   		= $mysqli->escape_string(date("l jS \of F Y h:i:s A"));
    $image       	= "image";
    $status			= true;

    $sql = "INSERT INTO productos (name,price,quantity,description, fecha_purchase,img_product,status)" 
            . "VALUES ('$nombre','$precio','$cantidad','$descripcion',NOW(), '$image' , true)";


    if ( $mysqli->query($sql) ){

       echo "Bien";

    }
    else {

        echo $mysqli->error;
  
    }*/

?>