<?php
	/* User login process, checks if user exists and password is correct */
	session_start();
	// Escape email to protect against SQL injections
	$email  = $mysqli->escape_string($_POST['email']);
	$result = $mysqli->query("SELECT * FROM usuarios WHERE email='$email'");

	if ( $result->num_rows == 0 ){ 
	    //Aqui en el usuario No existe
	    //y devolvemos una variable de sesion con el siguiente mensaje
	    $_SESSION['message'] = "Este Usuario No existe!";

	    //si nos acordamos las variables $_SESSION se pueden usar en cualquier archivo php
	    //por eso se llama variables de session siempre viven en todo el codigo
	    header("location: ../mensajes/error.php");
	}
	else { 
	    // Si el usuario existe hacemos lo siguiente
	    // Creamos una variable user que contiene lo que se hizo en la consulta 
	    // $result linea 6
	    $user = $result->fetch_assoc();


	    //Pasamos a verificar la contraseña que se envio en POST
	    //<input type="email" required autocomplete="off" name="email"/> -- > INDEX.PHP linea 61
	    //<input type="password" required autocomplete="off" name="password"/> -- > INDEX.PHP linea 68

	    // password_verify() es una funcion de PHP Interna que nos permite validar la contraseña encriptada
	    // esta contraseña esta encriptada en la base de datos
	    if ( password_verify( $_POST['password'], $user['contrasena'] ) ) {
	        
	        //una vez verificada pasamos a crear las variables de session 
	        //con toda la informacion del usuario ya creada
	        $_SESSION['email']      = $user['email'];
	        $_SESSION['nombres']    = $user['nombres'];
	        $_SESSION['apellidos']  = $user['apellidos'];
	        $_SESSION['active']     = $user['active'];
	        
	        // Así es como sabremos que el usuario está conectado
	        $_SESSION['logged_in'] = true;

	        echo $_SESSION['email'];
	        //una vez creada las variables de session lo redirigimos a la pagina profile.php
	        header("location: http://localhost/muebles/Vista/index2.php?pag=adminCrearUsuario&menu=1&foo=2"); 
	    }
	    else {

	        //En caso de que la contraseña este mal se envia a la pagina error.php con el mensaje
	        $_SESSION['mensaje'] = "Contraseña/Usuario inválido, intenta de nuevo!";

	        header("location: http://localhost/muebles/Vista/mensajes/error.php");
	    }
	}

?>