-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-12-2017 a las 14:47:32
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `muebles`


CREATE TABLE `compras` (
  `id` int(11) NOT NULL,
  `cedula` int(50) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `observations` varchar(200) NOT NULL,
  `quantity` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` int(50) NOT NULL,
  `quantity` int(50) NOT NULL,
  `fecha_purchase` date NOT NULL,
  `description` varchar(200) NOT NULL,
  `img_product` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cliente` (
  `name` varchar(50) NOT NULL,
  `cedula` int(50) NOT NULL,
  `fecha` date NOT NULL,
  `observations` varchar(200) NOT NULL,
  `producto_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `productos` (`id`, `name`, `price`, `quantity`, `fecha_purchase`, `description`, `img_product`, `status`) VALUES
(9, 'Pruebas 1', 123123123, 10, '2017-12-14', 'asdasdasd', 'near2.png', 1),
(10, 'Pruebas', 213123, 21312, '2017-12-15', 'asdasd', 'New-2016-Ghost-Recon-Wildlands-4K-Wallpaper.jpg', 1),
(14, 'sadasd', 23123, 231123, '2017-12-15', 'sadasdasd', 'simple.jpg', 1),
(15, 'adasd', 23123, 123123, '2017-12-15', 'asdasdasd', 'mount.jpg', 1);

CREATE TABLE `solicitud` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido` varchar(50) NOT NULL,
  `telefono` int(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `img` varchar(32) NOT NULL,
  `description` varchar(200) NOT NULL,
  `fecha_solicitud` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `usuarios` (
  `cedula` int(11) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `usuarios` (`cedula`, `nombres`, `apellidos`, `email`, `contrasena`, `hash`, `active`) VALUES
(123123, 'asdasd', 'asdasd', 'cristian@asdasd.com', '$2y$10$3zSyyO/VicB2JET6ARuIMuZutmgO8VkKsFj81rjqqsNFlj6f5HCbO', '19f3cd308f1455b3fa09a282e0d496f4', 0),
(6546465, 'asdhasdj', 'asjdgasjd', 'cristian@asdasd.coasdas', '$2y$10$hZWI8maisE10h2pJSWibTO5GuNFr20eQHf73EU4SlQUyKic9WkzqW', '8fe0093bb30d6f8c31474bd0764e6ac0', 0),
(1088315360, 'Christian', 'Jimenez', 'cristian@vifuy.com', '$2y$10$GmQpDDWFZaCBQIjS9drwKugP9PUjXhQkfRNU7bim3B.k9C49srjBW', '389bc7bb1e1c2a5e7e147703232a88f6', 0);

ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cedula`),
  ADD KEY `producto_id` (`producto_id`);

ALTER TABLE `compras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `producto_id` (`producto_id`);


ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cedula`);


ALTER TABLE `compras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;


ALTER TABLE `solicitud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`);

ALTER TABLE `compras`
  ADD CONSTRAINT `compras_ibfk_1` FOREIGN KEY (`producto_id`) REFERENCES `productos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
