<?php
  $url = $_SERVER['DOCUMENT_ROOT']."/muebles/";
  require $url.'Modelo/conexionBasesDatos.php';
  require $url."Modelo/Producto.php";

  
?>
<script type="text/javascript" src="../Js/producto.js"></script>
<script type="text/javascript" src="../Js/mensajes.js"></script>

  <?php
    extract ($_REQUEST);
    $mensaje;
    if(isset($_REQUEST['msj'])){

      if ($_REQUEST['msj'] == 9 ) {
        echo "<script> mensaje(1); </script> ";
      }else if($_REQUEST['msj'] == 2) {
        echo "<script> mensaje(2); </script> ";
      }else if($_REQUEST['msj'] == 3) {
        echo "<script> mensaje(3); </script> ";
      }else if($_REQUEST['msj'] == 4) {
        echo "<script> mensaje(4); </script> ";
      }else if($_REQUEST['msj'] == 5) {
        echo "<script> mensaje(5); </script> ";
      }else if($_REQUEST['msj'] == 6) {
        echo "<script> mensaje(6); </script> ";
      }else if($_REQUEST['msj'] == 7) {
        echo "<script> mensaje(7); </script> ";
      }else if($_REQUEST['msj'] == 8) {
         echo "<script> mensaje(8); </script> ";
      }
    }
  ?>

<div id="formularioAgregarProducto" class="row center style-form-admin" style=" margin-left: 25%!important;">
    <div align="center" >

      <h3>Editar producto</h3>
      
    </div>
    <div class="row">

      <?php
        extract ($_REQUEST);
        if(isset($_REQUEST['producto'])){
          $objProducto = new Producto();
          $producto = $objProducto->consultarProducto($_REQUEST['producto']);

          while($registro=$producto->fetch_object())
          {
            echo '<form action="http://localhost/muebles/Controlador/validarEditarProducto.php?producto='. $registro->id .'" method="post" class="col s12" enctype="multipart/form-data">
                    <div class="row">
                      <div class="input-field col s6">
                        <input id="nombre" type="text" name="nombre" class="validate" value="'. $registro->name .'" required>
                        <label for="first_name">Nombre</label>
                      </div>
                      <div class="input-field col s6">
                        <input id="precio" type="number" name="precio" class="validate" value="'. $registro->price .'" required>
                        <label for="precio">Precio</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="input-field col s12">
                        <input id="cantidad" type="number" name="cantidad" class="validate" value="'. $registro->quantity .'" required>
                        <label for="cantidad">Cantidad</label>
                      </div>
                    </div>

                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="descripcion" name="descripcion" class="materialize-textarea" required> '. $registro->description .' </textarea>
                        <label for="textarea1">Descripción</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="file-field input-field">
                        <div class="btn" style="background: #4BAECE;">
                          <span>Imagen del producto</span>
                          <input id="imagen" name="imagen" type="file" >
                        </div>

                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                        </div>

                        <img src="http://localhost/muebles/uploads/'. $registro->img_product .'" style="width:30%" >
                      </div>
                    </div>
                    </div>
                    <button class="waves-effect waves-light btn-large center" type="submit" name="submit" style="width: 100%; background: #4BAECE; ">Guardar</button>
                  </form>';
          }
        }
      ?>
      
    </div>
</div>