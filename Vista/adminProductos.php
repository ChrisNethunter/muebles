<?php
  $url = $_SERVER['DOCUMENT_ROOT']."/muebles/";
  require $url.'Modelo/conexionBasesDatos.php';
  require $url."Modelo/Producto.php";

  $objProducto = new Producto();
  $productos = $objProducto->consultarProductos();
?>
<script type="text/javascript" src="../Js/producto.js"></script>
<script type="text/javascript" src="../Js/mensajes.js"></script>
<div class="container">
  <a  id="agregar-producto" class="waves-effect waves-light btn-large center" style="margin-top: 5%;width: 107%; background: #4BAECE; margin-left: 10%; ">Crear Producto</a>
</div>

<div id="formularioAgregarProducto" class="row center style-form-admin" style="display:none; margin-left: 25%!important;">
    <div align="center" >

      <h3>Ingresar producto</h3>
      
    </div>
    <div class="row">
      <form action="http://localhost/muebles/Controlador/validarInsertarProducto.php" method="post" class="col s12" enctype="multipart/form-data">
        <div class="row">
          <div class="input-field col s6">
            <input id="nombre" type="text" name="nombre" class="validate" required>
            <label for="first_name">Nombre</label>
          </div>
          <div class="input-field col s6">
            <input id="precio" type="number" name="precio" class="validate" required>
            <label for="precio">Precio</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input id="cantidad" type="number" name="cantidad" class="validate" required>
            <label for="cantidad">Cantidad</label>
          </div>
        </div>
        <div class="row">
          <div class="file-field input-field">
            <div class="btn" style="background: #4BAECE;">
              <span>Imagen del producto</span>
              <input id="imagen" name="imagen" type="file" required>
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
            </div>
          </div>
        </div>
          <div class="row">
            <div class="input-field col s12">
              <textarea id="descripcion" name="descripcion" class="materialize-textarea" required></textarea>
              <label for="textarea1">Descripción</label>
            </div>
          </div>
          
        </div>
        <button class="waves-effect waves-light btn-large center" type="submit" name="submit" style="width: 100%; background: #4BAECE; ">Guardar</button>
      </form>
    </div>
</div>


<div id="tabla-productos" class="row">

  <div id="mensajes" class="valign-wrapper" style="margin-left: 43%; margin-left: 49%;margin-top: 25px;">

    <?php
      extract ($_REQUEST);
      $mensaje;
      if(isset($_REQUEST['msj'])){

        if ($_REQUEST['msj'] == 1 ) {
          echo "<script> mensaje(1); </script> ";
        }else if($_REQUEST['msj'] == 2) {
          echo "<script> mensaje(2); </script> ";
        }else if($_REQUEST['msj'] == 3) {
          echo "<script> mensaje(3); </script> ";
        }else if($_REQUEST['msj'] == 4) {
          echo "<script> mensaje(4); </script> ";
        }else if($_REQUEST['msj'] == 5) {
          echo "<script> mensaje(5); </script> ";
        }else if($_REQUEST['msj'] == 6) {
          echo "<script> mensaje(6); </script> ";
        }else if($_REQUEST['msj'] == 7) {
          echo "<script> mensaje(7); </script> ";
        }else if($_REQUEST['msj'] == 8) {
           echo "<script> mensaje(8); </script> ";
        }
      }
    ?>

  </div>
  <table class="responsive-table col s9 " style="margin-left: 22% !important;" >
    <tr>
      <th>Producto</th>
      <th>Precio</th>
      <th>Cantidad</th>
      <th>Descripción</th>
      <th>Fecha</th>
      <th>Imagen</th>
      <th>Estado</th>
      <th>Modificar</th>
      <th>Eliminar</th>
    </tr>
    <?php
      while($registro=$productos->fetch_object())
      {
        ?>
          <tr>
            <td><?php echo $registro->name?></td>
            <td><?php echo $registro->price?></td>
            <td><?php echo $registro->quantity?></td>
            <td><?php echo $registro->description?></td>
            <td><?php echo $registro->fecha_purchase?></td>
            <td><?php echo "<img src='http://localhost/muebles/uploads/". $registro->img_product ."' style='width: 30%;'/ "?></td>
            <td>
              
              <?php 
                if ($registro->status == 1) {
                  echo '<a class="waves-effect waves-light btn  green">Activo</a>';
                }else{
                  echo '<a class="waves-effect waves-light btn  green">Inactivo</a>';
                }
              ?>
            </td>
            
            <td>
              <?php 
                echo '<a id='. $registro->id .' href="http://localhost/muebles/Vista/index2.php?pag=adminEditarProducto&menu=1&producto='. $registro->id .'" class="update" title="Modificar Producto" style="cursor:pointer; color: #4c4b4b;"><i class="material-icons">mode_edit</i></a>';
              ?>
            </td>
            <td>
              <?php 
                echo '<a id='. $registro->id .' href="http://localhost/muebles/Controlador/validarEliminarProducto.php?producto='.$registro->id.'" class="delete" title="Eliminar" style="cursor:pointer; color: #4c4b4b;"><i class="material-icons">delete</i></a>'; 
              ?>
            </td>
            
          </tr>  
        <?php
      }  //cerrando el ciclo while
    ?>
    
  </table>
</div>

