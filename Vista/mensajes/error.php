<?php
    /* Aqui mostramos todos los mensajes de error */
    session_start();
?>
<!DOCTYPE html>
    <html>
    <head>
        <title>Error</title>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <link rel="stylesheet" type="text/css" href="../../public/css/estilos.css" >
        <link rel="shortcut icon" href="../../public/img/sofa.png" />
    </head>
    <body>
        <div class="form">
            <h1>Error</h1>
            <p>
                <?php
                    //Aqui verificamos que hay mensajes
                    // isset() = Determina si una variable está definida y no es NULL.
                    // empty() =Determina si una variable es considerada vacía. Una variable se considera vacía 
                    // si no existe o si su valor es igual a FALSE. empty() no genera una advertencia si la variable no existe..
                    if( isset($_SESSION['message']) AND !empty($_SESSION['message']) ): 
                        echo $_SESSION['message'];    
                    else:
                        header( "location: ../iniciosesion/inicarSesion.php" );
                    endif;
                ?>
            </p>
            <!-- Aqui el boton rederige a index.php -->     
            <a href="http://localhost/muebles/Vista/iniciosesion/iniciarSesion.php"><button class="button button-block"/>Home</button></a>
        </div>
    </body>
</html>