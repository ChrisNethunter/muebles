<?php 
  $url = $_SERVER['DOCUMENT_ROOT']."/muebles/";
  require $url.'Modelo/conexionBasesDatos.php';
  require $url.'Modelo/solicitud.php';

  $objSolicitud = new Solicitud();
  $solicitud = $objSolicitud->consultarSolicitudes();
?>

<div id="tabla-solicitud" class="row">
  <table class="responsive-table col s9 " style="margin-left: 22% !important; position: relative; top: 20px;" >
    <tr>
      <th>Nombres</th>
      <th>Apellidos</th>
      <th>Telefono</th>
      <th>Email</th>
      <th>imagen</th>
      <th>Descripcòn</th>
      <th>Fecha de solicitud</th>
    </tr>
    <?php
      while($registro=$solicitud->fetch_object())
      {
        ?>
          <tr>
            <td><?php echo $registro->nombre?></td>
            <td><?php echo $registro->apellido?></td>
            <td><?php echo $registro->telefono?></td>
            <td><?php echo $registro->email?></td>
            <td><?php echo "<img src='http://localhost/muebles/uploads/solicitudes/". $registro->img ."' style='width: 30%;'/ "?></td>
            <td><?php echo $registro->description?></td>
            <td><?php echo $registro->fecha_solicitud?></td>
            <td>
              
              <?php 
                if ($registro->status == 1) {
                  echo '<a class="waves-effect waves-light btn  green">Activo</a>';
                }else{
                  echo '<a class="waves-effect waves-light btn  green">Inactivo</a>';
                }
              ?>
            </td>
            
            <td>
              <?php 
                echo '<a id='. $registro->id .' class="update" title="Modificar Producto" style="cursor:pointer; color: #4c4b4b;"><i class="material-icons">mode_edit</i></a>';
              ?>
            </td>
            <td>
              <?php 
                echo '<a id='. $registro->id .' href="http://localhost/muebles/Controlador/validarEliminarProducto.php?producto='.$registro->id.'" class="delete" title="Eliminar" style="cursor:pointer; color: #4c4b4b;"><i class="material-icons">delete</i></a>'; 
              ?>
            </td>
            
          </tr>  
        <?php
      }  //cerrando el ciclo while
    ?>
      
  </table>
</div>
</div>