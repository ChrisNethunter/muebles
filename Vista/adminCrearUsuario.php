<?php 
  $url = $_SERVER['DOCUMENT_ROOT']."/muebles/";
  require $url.'Modelo/conexionBasesDatos.php';
  require $url.'Modelo/usuario.php';

  $objUsuarios = new Usuario();
  $usuario = $objUsuarios->consultarUsuarios();
?>

<?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST') 
  {   
    if (isset($_POST['login'])) { 
      require $url.'Controlador/validarInicioSesion.php';
    }
    elseif (isset($_POST['register'])) { 
      require 'iniciosesion/registrarCuenta.php';
    }
  }
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

  <meta charset="UTF-8">
  <link rel="shortcut icon" href="../../public/img/sofa.png" />
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  
</head>
<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="../Js/usuario.js"></script>
  <div class="container">
    <a id="agregar-usuario" class="waves-effect waves-light btn-large center" style="margin-top: 5%;width: 107%; background: #4BAECE; margin-left: 10%; ">Crear Usuario</a>
  </div>

  <div id="formulario-usuario" style="width: 70%;  margin-left: 25%; display: none;">   
    <h1>Registrar Usuario</h1>
    
    <form action="adminCrearUsuario.php" method="post" autocomplete="off">
      <div class="field-wrap">
          <label>
            C.C<span class="req">*</span>
          </label>
          <input type="number" required autocomplete="off" name='cedula' />
      </div>
    
      <div class="top-row">
        <div class="field-wrap">
          <label>
            Nombres<span class="req">*</span>
          </label>
          <input type="text" required autocomplete="off" name='firstname' />
        </div>
    
        <div class="field-wrap">
          <label>
            Apellidos<span class="req">*</span>
          </label>
          <input type="text"required autocomplete="off" name='lastname' />
        </div>
      </div>

      <div class="field-wrap">
        <label>
          Correo Electronico<span class="req">*</span>
        </label>
        <input type="email"required autocomplete="off" name='email' />
      </div>
      
      <div class="field-wrap">
        <label>
          Contraseña<span class="req">*</span>
        </label>
        <input type="password"required autocomplete="off" name='password'/>
      </div>
      
      <button type="submit" class="button button-block" name="register" />Registrar</button>
    
    </form>
  </div> 
</body>


<!--SIGNUP FORM-->   

<!-- tab-content -->   
<br>
<br>
<br>
<div id="tabla-usuarios" class="row">

  <div id="mensajes" class="valign-wrapper" style="margin-left: 43%; margin-left: 49%;margin-top: 25px;">

    <?php
      extract ($_REQUEST);
      $mensaje;
      if(isset($_REQUEST['msj'])){

        if ($_REQUEST['msj'] == 1 ) {
          echo '<h6 class="center-align" style="color: green;">Se Guardo el Usuario Correctamente</h6>';
        }else if($_REQUEST['msj'] == 2) {

          echo '<h6 class="center-align" style="color: red;">Error, Intenta de Nuevo</h6>';
        };
      }
    ?>

  </div>
  <table class="responsive-table col s9 " style="margin-left: 22% !important;" >
    <tr>
      <th>Cedula</th>
      <th>Nombres</th>
      <th>Apellidos</th>
      <th>correo</th>
    </tr>
    <?php
      while($registro=$usuario->fetch_object())
      {
        ?>
          <tr>
            <td><?php echo $registro->cedula?></td>
            <td><?php echo $registro->nombres?></td>
            <td><?php echo $registro->apellidos?></td>
            <td><?php echo $registro->email?></td>
            <td>
              
              <?php 
                if ($registro->active == 1) {
                  echo '<a class="waves-effect waves-light btn  green">Activo</a>';
                }else{
                  echo '<a class="waves-effect waves-light btn  green">Inactivo</a>';
                }
              ?>
            </td>
            
            <td>
              <?php 
                echo '<a id='. $registro->cedula .' class="update" title="Modificar Producto" style="cursor:pointer; color: #4c4b4b;"><i class="material-icons">mode_edit</i></a>';
              ?>
            </td>
            <td>
              <?php 
                echo '<a id='. $registro->cedula .' href="http://localhost/muebles/Controlador/validarEliminarProducto.php?producto='.$registro->cedula.'" class="delete" title="Eliminar" style="cursor:pointer; color: #4c4b4b;"><i class="material-icons">delete</i></a>'; 
              ?>
            </td>
            
          </tr>  
        <?php
      }  //cerrando el ciclo while
    ?>
      
  </table>
</div>
</div>