<?php
	
	extract ($_REQUEST);
	session_start();
	//$pag  = 'adminCompras';
	//echo "request " . print_r($_REQUEST) ;
	//echo "Pruebas"  . $_REQUEST['pag'];	
	$footer = '';
	/*if (!isset($_SESSION['user']))
		header("location:../index.php?x=2");//x=2 significa que no han iniciado sesión*/
	if (!isset( $_REQUEST['pag'] ) ){
		$pag='contenido';
	}
		
	/*if (!isset($_REQUEST['msj'])){
		$msj=0;
	}*/

	if($_REQUEST['menu'] == 1){

		$menu = 'adminMenu';

	}else if($_REQUEST['menu'] == 2) {

		$menu = 'menu';

	}else{
		$menu = '';
	}
	if (isset( $_REQUEST['foo'] ) ){
		if ($_REQUEST['foo'] == 1){
			$footer = 'footer';
		}
	}
	
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title>Muebles</title>

		<link rel="shortcut icon" href="../public/img/sofa.png" />
		<link rel="stylesheet" type="text/css" href="../public/css/style.css" >
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
		 <!--Import Google Icon Font-->
      	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
	
	<script src="https://unpkg.com/sweetalert2@7.2.0/dist/sweetalert2.all.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

	
	<body>
		<!--<div id="divMenu"> 
			<?php /*include "menu.php"*/ ?> 
		</div>-->
		<header id="divMenu"> 
			<?php 
				if ($menu != "") {
					include $menu.".php"; 					
				}
			?>
		</header>
		    
			
		<main>

			<?php 
				include $pag.".php" 
			?> 

		</main>

		<footer>
			<?php
				if ($footer != "") {
					include $footer.".php";			
				} 
			
			?> 
		</footer>
		
		
	</body>
	
</html>