<div class="row center"  style="width: 70%;">
  	<div align="center" >
  		<h1>Escríbenos</h1>
  		
  	</div>
  	<div class="row col s6" style="">
	    <form class="col s12"   >
	      
	        <div class="input-field col s12">
	          <input id="first_name" type="text" class="validate">
	          <label for="first_name">Nombre</label>
	        </div>
		      
	        <div class="input-field col s12">
	          	<input id="tel" type="number" class="validate">
	          	<label for="telefono">Telefono</label>
	        </div>
	        <div class="row">
	        	<div class="input-field col s12">
	          		<input id="email" type="email" class="validate">
	          		<label for="email">Email</label>
	        	</div>
	      	</div>
	     	<div class="input-field col s12">
		        <textarea id="textarea1" class="materialize-textarea"></textarea>
		        <label for="textarea1">Mensaje</label>
		    </div>
	     
		    <div>
		    	<a class="waves-effect waves-light btn-large left" style="background: #4BAECE;">enviar</a>	
		    </div>
	        	
	    </form>
	</div>
    <div class="col s6" style="display: inline-block; text-align: left; padding-left: 100px; ">

		<h5>Contacto</h5>
		<p>Lunes-viernes 2:00 PM - 7:00 PM</p>

		<div class="phone mt-40">
			<i class="icon_phone"></i>
			<h5>Telefono</h5>
			<p>+57 3202020</p>
		</div>						

		<div class="email mt-40">
			<i class="icon_mail"></i>
			<h5>Email</h5>
			<a href="mailto:comodin@gmail.com">comodin@gmail.com</a>
		</div>						

		<div class="address mt-40">
			<i class="icon_pin"></i>
			<h5>Dirección</h5>
			<p>Edifio 5<br>Pereira<br>Colombia</p>
		</div>						

	</div>

</div>