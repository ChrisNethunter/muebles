<?php 
  /* Pagina para Login y Registro */
  $url = $_SERVER['DOCUMENT_ROOT']."/muebles/";
  require $url.'Modelo/conexionBasesDatos.php';
  //session_start();
?>

<?php
  //Aqui validamos el metodo post para saber que se ha enviado cualquier formulario
  if ($_SERVER['REQUEST_METHOD'] == 'POST') 
  {   
    //Validamos si fue el formulario login o registro
    //como hacemos eston ? 
    //<button class="button button-block" name="login" />Log In</button>
    //Los botones del login tienen el name que hace referencia
    //con eso nos damos cuenta si fue login o register
    //<button type="submit" class="button button-block" name="register" />Register</button>

    if (isset($_POST['login'])) { 
      //user logging in
      //Al saber que es login incluimos el codigo en login.php para validar si existe
      //login.php hace la conexion a la base de datos directamente
      //si lo ven no hay un require de db.php
      //por que ?
      //porque si miramos la linea 3 de este archivo require 'db.php'; ya lo tenemos
      //lo unico que hacemos es incluir codigo desde aqui para abajo
      //lo mismo ocurre para register
      //echo "LOGIN FORM";
      //echo "Login";
      require $url.'Controlador/validarInicioSesion.php';
    }
    elseif (isset($_POST['register'])) { 
      //user registering
      //echo "REGISTER FORM";
      require 'registrarCuenta.php';
    }
  }
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>

    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="shortcut icon" href="../../public/img/sofa.png" />
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" type="text/css" href="../../public/css/estilos.css" >
    
  </head>
  <body>

    <div class="form">
        <ul class="tab-group">
          <!--<li class="tab"><a href="#signup">Sign Up</a></li>-->
          <li class="tab active"><a href="#login">Iniciar sesiòn</a></li>
        </ul>
        
        <div class="tab-content">
          <!--LOGIN FORM-->
          <div id="login">   
            <h1>Bienvenidos a Comodin</h1>
            
            <form action="http://localhost/muebles/Vista/iniciosesion/iniciarSesion.php" method="post" >
            
              <div class="field-wrap">
              <label>
                Correo<span class="req">*</span>
              </label>
              <input type="email" required autocomplete="off" name="email"/>
            </div>
            
            <div class="field-wrap">
              <label>
                Contraseña<span class="req">*</span>
              </label>
              <input type="password" required autocomplete="off" name="password"/>
            </div>
            
            <p class="forgot"><a href="olvidarContraseña.php">Olvide la contraseña?</a></p>
            
            <button class="button button-block" name="login" />Log In</button>
            
            </form>

          </div>
          <!--LOGIN FORM-->

          <!--SIGNUP FORM-->   
          <div id="signup">   
            <h1>Sign Up for Free</h1>
            
            <form action="iniciarSesion.php" method="post" autocomplete="off">

              <div class="field-wrap">
                  <label>
                    C.C<span class="req">*</span>
                  </label>
                  <input type="number" required autocomplete="off" name='cedula' />
              </div>
            
              <div class="top-row">
                <div class="field-wrap">
                  <label>
                    First Name<span class="req">*</span>
                  </label>
                  <input type="text" required autocomplete="off" name='firstname' />
                </div>
            
                <div class="field-wrap">
                  <label>
                    Last Name<span class="req">*</span>
                  </label>
                  <input type="text"required autocomplete="off" name='lastname' />
                </div>
              </div>

              <div class="field-wrap">
                <label>
                  Email Address<span class="req">*</span>
                </label>
                <input type="email"required autocomplete="off" name='email' />
              </div>
              
              <div class="field-wrap">
                <label>
                  Set A Password<span class="req">*</span>
                </label>
                <input type="password"required autocomplete="off" name='password'/>
              </div>
              
              <button type="submit" class="button button-block" name="register" />Register</button>
            
            </form>

          </div>  
          <!--SIGNUP FORM-->   
          
        </div><!-- tab-content -->    
    </div> <!-- /form -->

  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="../../Js/login.js"></script>
</html>