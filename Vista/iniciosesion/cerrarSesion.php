<?php
	/* Aqui destruimos las variables de session */
	session_start();
	session_unset();
	session_destroy(); 
?>
<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="UTF-8">
	  	<title>Cerrar Sesion</title>
	  	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	    <link rel="stylesheet" type="text/css" href="../../public/css/estilos.css" >
	</head>

	<body>
	    <div class="form">

          <h1>Thanks for stopping by</h1>
              
          <p><?= 'You have been logged out!'; ?></p>
          <!-- Aqui el boton rederige a index.php --> 
          <a href="http://localhost/muebles/Vista/index2.php?pag=Portada&menu=2&foo=1"><button class="button button-block"/>Home</button></a>

	    </div>
	</body>
</html>