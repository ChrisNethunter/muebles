<!DOCTYPE html>
<html>
  <head>
    <title>¿Has olvidado tu contraseña?</title>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" type="text/css" href="../../public/css/estilos.css" >
  </head>

  <body>
      
    <div class="form">
      <h1>Reset Your Password</h1>

      <form action="forgot.php" method="post">
       <div class="field-wrap">
        <label>
          Email Address<span class="req">*</span>
        </label>
        <input type="email"required autocomplete="off" name="email"/>
      </div>
      <button class="button button-block"/>Reset</button>
      </form>
    </div>
            
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script type="text/javascript" src="../../Js/login.js"></script>
  </body>
</html>