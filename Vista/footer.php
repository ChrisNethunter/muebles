<footer class="page-footer cyan darken-3">
  <div class="container">
    <div class="row">
      <div class="col 4 s4">
        <h5 class="white-text">Frase</h5>
        <p class="grey-text text-lighten-4">“No te quedes sentado esperando que lleguen las cosas a ti. Lucha por lo que quieres, hazte responsable de ti mismo”.
        <br>
        <br> 
        Autor:  Michel Tanus</p>
      </div>
      <div class="col 4 s4">
        <h5 class="white-text">Unete</h5>
        <p class="grey-text text-lighten-4">“No te quedes sentado esperando que lleguen las cosas a ti. Lucha por lo que quieres, hazte responsable de ti mismo”.
        <br>
        <br> 
        Autor:  Michel Tanus</p>
      </div>
      <div class="col l2 offset-l2 s12">
        <h5 class="white-text">Visitanos</h5>
        <ul>
          <li><a class="grey-text text-lighten-3" href="#!"><img src="../public/img/twitter.png" style="width: 30px; margin-left: 5px; position: relative; top: 6px;">   Twitter</a></li>
          <li><a class="grey-text text-lighten-3" href="#!"><img src="../public/img/Facebook.png" style="width: 53px; position: relative; margin-left: -8px; position: relative; top: 20px;">Facebook</a></li>
          <li><a class="grey-text text-lighten-3" href="#!"><img src="../public/img/instagram.png" style="width: 40px; position: relative; top: 20px;">  Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    © 2017 Comodin
    <a class="grey-text text-lighten-4 right" href="#!">www.comodin.com</a>
    </div>
  </div>
</footer>