
<!--<script type="text/javascript" src="../Js/producto.js"></script>-->
<div class="row center"  style="width: 70%;">
  	<div align="center" >
  		<h1>Compramos</h1>
  		<p class="text-portada">Para realizar su venta envianos la imagen del producto y los datos respectivos y estaremos en contacto lo mas pronto posible</p>
  	</div>
  	<div class="row " >
	    <form action="http://localhost/muebles/Controlador/validarInsertarSolicitud.php" method="post"  enctype="multipart/form-data" class="col s12">
		    <div class="row">
		        <div class="input-field col s6">
		          <input id="nombre" name="nombre" type="text" class="validate" required>
		          <label for="nombre">Nombre</label>
		        </div>
		        <div class="input-field col s6">
		          <input id="apellido" name="apellido" type="text" class="validate" required>
		          <label for="apellido">Apellidos</label>
		        </div>
		    </div>
		      
		    <div class="row">
		        <div class="input-field col s6">
		          	<input id="telefono" name="telefono" type="number" class="validate" required>
		          	<label for="telefono">Telefono</label>
		        </div>
		        <div class="row">
		        	<div class="input-field col s6">
		          		<input id="email" name="email" type="email" class="validate" required>
		          		<label for="email">Email</label>
		        	</div>
		      	</div>
		    </div>
		    <div class="row">

		      	<div class="row">
				    <div class="file-field input-field">
				      <div class="btn" style="background: #4BAECE;">
				        <span>File</span>
				        <input name="imagen" type="file" required>
				      </div>
				      <div class="file-path-wrapper">
				        <input id="imagen" class="file-path validate" type="text">
				      </div>
				    </div>
		      	</div>
		        <div class="row">
			        <div class="input-field col s12">
			          <textarea id="descripcion" name="descripcion" class="materialize-textarea" required></textarea>
			          <label for="descripcion">Descripcion</label>
			        </div>
			    </div>
		        
		    </div>
		    <button class="waves-effect waves-light btn-large center" type="submit" name="submit" style="width: 100%; background: #4BAECE; ">Enviar</button>

		    <?php
	
				extract ($_REQUEST);

				if(  isset($_REQUEST['msj']) ){

					if($_REQUEST['msj'] == 1){

						echo '<h4 style="color: green"> Exito </h4>';

					}else if($_REQUEST['msj'] == 2) {

						echo '<h4 style="color: red"> Exito </h4>';

					}
					
				}
			?>
		    
	    </form>
	</div>
</div>
