<?php
	$url = $_SERVER['DOCUMENT_ROOT']."/muebles/";
  	require $url.'Modelo/conexionBasesDatos.php';
  	require $url."Modelo/Producto.php";

  	$objProducto= new Producto();

  	$productos = new Producto();
  	$productos = $objProducto->consultarProductos();

?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div>

	<?php
      while($registro=$productos->fetch_object())
      {
       
		echo '<div class="cont-cart card">
			    <div class="card-image waves-effect waves-block waves-light">
			      <img class="activator" src="../uploads/'. $registro->img_product . '">
			    </div>
			    <div class="card-content">
			      <span class="card-title activator grey-text text-darken-4">'.$registro->name.'<i class="material-icons right">more_vert</i></span>
			      <p><a href="#"></a></p>
			    </div>
			    <div class="card-reveal">
			      <span class="card-title grey-text text-darken-4">'.$registro->name.'<i class="material-icons right">close</i></span>
			      <p>'.$registro->description.'</p>
			      <p> Cantidad Disponible : '.$registro->quantity.'</p>
			      <a id="'. $registro->id .'" class="waves-effect waves-light btn-large center" style="background: #4BAECE;">Comprar</a>
			    </div>
			  </div>';
      }  //cerrando el ciclo while
    ?>
	
	
</div>